<?php
/* 
  Doplňte kód tak, aby zobrazil z DB seznam obcí dle id okresu. Id okresu doplňte do SQL na pevno, 
  není potřeba zadávat pomocí formuláře
*/

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "kraje_okresy";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql="SET CHARACTER SET UTF8";
$conn->query($sql);

$id = 1;
$sql = "SELECT nazev FROM obec WHERE okres_id='.$id.'";

$result = $conn->query($sql);

if ($result->num_rows > 0) {
  while($row = $result->fetch_array()){
    echo $row["nazev"]."\n";
  }
  
} else {
  echo "0 results";
}
$conn->close();
?>