<?php
/* 
  Doplňte kód tak, aby zobrazil z DB seznam okresů dle id kraje. Id kraje přijde ze souvisejícího 
  HTML formuláře (VyberKrajeForm.html).
*/

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "kraje_okresy";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql="SET CHARACTER SET UTF8";
$conn->query($sql);

$id = $_POST["nazev"];

$sql = "SELECT nazev FROM okres WHERE kraj_id='.$id.'";

$result = $conn->query($sql);

if ($result->num_rows > 0) {
  // output data of each row
  while($row = $result->fetch_array()){
    echo $row["nazev"]."\n";
  }
} else {
  echo "0 results";
}
$conn->close();
?>